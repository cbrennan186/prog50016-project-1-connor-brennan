#include <iostream>
#include <SFML/System.hpp>
#include<SFML/Graphics.hpp>
#include <string> 
#include <iostream>
#include <fstream>
#include "json.hpp"
#include "Enemy.h"
#include "Player.h"
#include "Meteor.h"

/// <summary>
/// This is the header file for all window related data
/// </summary>
class Window
{
public:
	Window();
	~Window();
	void RunGame();
	void Initialize();

private:
	int windowXAxis;
	int windowYAxis;
	int characterSize;
	sf::RenderWindow* window;
	sf::View view;
	float halfwidth;
	float halfheight;
    sf::Texture backgroundTexture;
    sf::Texture playerTexture;
    sf::Texture enemyTexture;
    sf::Texture playerShotTexture;
    sf::Texture enemyShotTexture;
    sf::Texture playerDeadTexture;
    sf::Texture explostionTexture;
    sf::Texture smallMeteorTexture;
    sf::Texture largeMeteorTexture;
    sf::Sprite backgroundSprite;
    sf::Sprite playerSprite;
    sf::Sprite enemySprite;
    sf::Sprite playerShotSprite;
    sf::Sprite enemyShotSprite;
    sf::Sprite smallMeteorSprite;
    sf::Sprite largeMeteorSprite;
    sf::Font font;
    sf::Text livesText;
    Player* player;
    Enemy* enemy;
    Meteor* meteor;
    sf::Text gameOverText;
    sf::Text scoreText;
    sf::Text highScoreText;
    int playerShotCounter;
    int enemyShotCounter;
    bool shoot;
    bool playerLaserHitsEnemy;
    bool enemyLaserHitsPlayer;
    bool playerColideWithEnemy;
    bool enemyColideWithPlayer;
    bool smallAstroidHitsPlayer;
    bool largeAstroidHitsPlayer;
    bool playerLaserHitsSmallMeteor;
    bool playerLaserHitsLargeMeteor;
    bool ifGameOver;
    int gameOverCounter;
    sf::Event event;
    sf::CircleShape playerHitCircle;
    sf::CircleShape enemyHitCircle;
    sf::CircleShape enemyShootCircle;
    sf::CircleShape playerShootCircle;
    sf::CircleShape smallMeteorCircle;
    sf::CircleShape largeMeteorCircle;
    int livesNum;
    std::string livesString;
    int scoreNum;
    std::string scoreString;
    int highscoreNum;
    std::string highscoreString;
    bool playerDead;
    int playerRespawnCounter;
    bool enemyDead;
    int enemyRespawnCounter;
};

