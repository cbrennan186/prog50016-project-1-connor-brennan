#include "Meteor.h"

//This class contains all related info to the meteor class

/// <summary>
/// A constructor for the meator
/// </summary>
Meteor::Meteor()
{

}

/// <summary>
/// A destructor for the meteor
/// </summary>
Meteor::~Meteor()
{

}

/// <summary>
/// Sets the texture for the smaller meteor
/// </summary>
/// <param name="texture"></param>
void Meteor::SetSmallMeteorTexture(sf::Texture texture)
{
	smallMeteorTexture = texture;
}

/// <summary>
/// Sets the texture for the larger meteor
/// </summary>
/// <param name="texture"></param>
void Meteor::SetLargeMeteorTexture(sf::Texture texture)
{
	largeMeteorTexture = texture;
}