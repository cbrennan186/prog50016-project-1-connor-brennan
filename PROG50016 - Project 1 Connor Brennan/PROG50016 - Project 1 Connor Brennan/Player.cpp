#include "Player.h"

//This is the player class, it holds all related data regarding the player here


/// <summary>
/// Constructor
/// </summary>
Player::Player()
{

}

/// <summary>
/// Destructor
/// </summary>
Player::~Player()
{

}

/// <summary>
/// Sets the value for liveNums
/// </summary>
/// <param name="_livesNum"></param>
void Player::SetLivesNum(int _livesNum)
{
	livesNum = _livesNum;

}

/// <summary>
/// Sets the value for scoreNum
/// </summary>
/// <param name="_scoreNum"></param>
void Player::SetScoreNum(int _scoreNum)
{
	scoreNum = _scoreNum;
}

/// <summary>
/// Sets the string for lives
/// </summary>
/// <param name="_Lstring"></param>
void Player::SetLivesString(std::string _Lstring)
{
	livesString = _Lstring;
}

/// <summary>
/// Sets the value for the high score string
/// </summary>
/// <param name="_Hstring"></param>
void Player::SetHighscoreString(std::string _Hstring)
{
	highscoreString = _Hstring;
}

/// <summary>
/// sets the value for the score string 
/// </summary>
/// <param name="_Sstring"></param>
void Player::SetScoreString(std::string _Sstring)
{
	scoreString = _Sstring;
}

/// <summary>
/// Sets the value for hogh score num
/// </summary>
/// <param name="_hscorenum"></param>
void Player::SetHighscoreNum(int _hscorenum)
{
	highscoreNum = _hscorenum;
}

/// <summary>
/// Sets the value for the respawn counter
/// </summary>
/// <param name="_counter"></param>
void Player::SetRespawnCounter(int _counter)
{
	playerRespawnCounter = _counter;
}

/// <summary>
/// Sets the value for if the player is dead or not
/// </summary>
/// <param name="_pDead"></param>
void Player::SetPlayerDead(bool _pDead)
{
	playerDead = _pDead;
}