#include "Window.h"

//In the window class all other classes, and the main game loop are created and run here.

//Constructor 
Window::Window()
{

}

//Destructor 
Window::~Window()
{
    delete window;
    delete player;
    delete enemy;
    delete meteor;
}

/// <summary>
/// Run on start up, initializes all the data to be ready to be used 
/// </summary>
void Window::Initialize()
{
    windowXAxis = 1024;
    windowYAxis = 768;
    characterSize = 30;

    window = new sf::RenderWindow(sf::VideoMode(windowXAxis, windowYAxis), "Connor Brennans Space Shooter");
    window->setFramerateLimit(60);

    halfwidth = window->getSize().x * 0.5;
    halfheight = window->getSize().y * 0.5;
    view.reset(sf::FloatRect(-halfwidth, -halfheight, halfwidth * 2.0, halfheight * 2.0));
    window->setView(view);

    backgroundTexture.loadFromFile("prog50049.project1.resources/Background/starBackground.png");

    playerTexture.loadFromFile("prog50049.project1.resources/Mainplayer/player.png");

    enemyTexture.loadFromFile("prog50049.project1.resources/Enemies/enemyShip.png");

    playerShotTexture.loadFromFile("prog50049.project1.resources/Mainplayer/laserGreen.png");

    enemyShotTexture.loadFromFile("prog50049.project1.resources/Enemies/laserRed.png");

    smallMeteorTexture.loadFromFile("prog50049.project1.resources/Asteroids/meteorSmall.png");
    largeMeteorTexture.loadFromFile("prog50049.project1.resources/Asteroids/meteorBig.png");

    playerDeadTexture.loadFromFile("prog50049.project1.resources/Mainplayer/playerDamaged_2.png");

    explostionTexture.loadFromFile("prog50049.project1.resources/Misc/exp2_0.png");

    backgroundSprite.setTexture(backgroundTexture);
    backgroundSprite.setOrigin(sf::Vector2f(backgroundTexture.getSize().x * 0.5, backgroundTexture.getSize().y * 0.5));

    sf::Vector2f backgroundSize(windowXAxis, windowYAxis);

    backgroundSprite.setScale(
        backgroundSize.x / backgroundSprite.getLocalBounds().width,
        backgroundSize.y / backgroundSprite.getLocalBounds().height);

    playerSprite.setTexture(playerTexture);
    playerSprite.setOrigin(sf::Vector2f(playerTexture.getSize().x * 0.0, playerTexture.getSize().y * 0.0));

    enemySprite.setTexture(enemyTexture);
    enemySprite.setOrigin(sf::Vector2f(enemyTexture.getSize().x * 0.0, enemyTexture.getSize().y * 0.0));

    playerShotSprite.setTexture(playerShotTexture);
    playerShotSprite.setOrigin(sf::Vector2f(playerShotTexture.getSize().x * 0.0, playerShotTexture.getSize().y * 0.0));

    enemyShotSprite.setTexture(enemyShotTexture);
    enemyShotSprite.setOrigin(sf::Vector2f(enemyShotTexture.getSize().x * 0.0, enemyShotTexture.getSize().y * 0.0));

    smallMeteorSprite.setTexture(smallMeteorTexture);
    smallMeteorSprite.setOrigin(sf::Vector2f(smallMeteorTexture.getSize().x * 0.0, smallMeteorTexture.getSize().y * 0.0));

    largeMeteorSprite.setTexture(largeMeteorTexture);
    largeMeteorSprite.setOrigin(sf::Vector2f(largeMeteorTexture.getSize().x * 0.0, largeMeteorTexture.getSize().y * 0.0));

    if (font.loadFromFile("prog50049.project1.resources/Hud/cour.ttf") == false)
    {
        std::cout << "Could not find font" << std::endl;
    }

    livesNum = 3;
    scoreNum = 0;
    highscoreNum = 0;
    livesString = "Lives Left: " + std::to_string(livesNum);
    scoreString = "Score: " + std::to_string(scoreNum);
    highscoreString = "High Score: " + std::to_string(highscoreNum);

    livesText.setFont(font);
    livesText.setCharacterSize(characterSize);
    livesText.setFillColor(sf::Color::Green);
    livesText.setStyle(sf::Text::Regular);
    livesText.setString(livesString);
    livesText.setOrigin(sf::Vector2f(livesText.getLocalBounds().width * 2.1f, livesText.getLocalBounds().height * 19.5f));

    gameOverText.setFont(font);
    gameOverText.setCharacterSize(characterSize);
    gameOverText.setFillColor(sf::Color::Green);
    gameOverText.setStyle(sf::Text::Regular);
    gameOverText.setString("Game Over");
    gameOverText.setOrigin(sf::Vector2f(gameOverText.getLocalBounds().width * 0.0f, gameOverText.getLocalBounds().height * 0.0f));

    scoreText.setFont(font);
    scoreText.setCharacterSize(characterSize);
    scoreText.setFillColor(sf::Color::Green);
    scoreText.setStyle(sf::Text::Regular);
    scoreText.setString(scoreString);
    scoreText.setOrigin(sf::Vector2f(scoreText.getLocalBounds().width * 2.82f, scoreText.getLocalBounds().height * 17.8f));

    highscoreString = ("High Score: " + std::to_string(highscoreNum));

    highScoreText.setFont(font);
    highScoreText.setCharacterSize(30);
    highScoreText.setFillColor(sf::Color::Green);
    highScoreText.setStyle(sf::Text::Regular);
    highScoreText.setString(highscoreString);
    highScoreText.setOrigin(sf::Vector2f(highScoreText.getLocalBounds().width * 2.1f, highScoreText.getLocalBounds().height * 12.0f));

    std::ifstream inputStreamHighscore("./Assets/SaveData.json");
    std::string strHighscore((std::istreambuf_iterator<char>(inputStreamHighscore)), std::istreambuf_iterator<char>());
    json::JSON documentHighscore = json::JSON::Load(strHighscore);

    json::JSON subObjectHighscore = documentHighscore["subObjectPlayer"];

    highscoreNum = (subObjectHighscore["highscoreNum"].ToInt());

    highscoreString = ("High score: " + std::to_string(highscoreNum));

    highScoreText.setString(highscoreString);

    playerShotCounter = 0;
    enemyShotCounter = 0;
    shoot = false;

    smallMeteorSprite.setPosition(150, -350);
    largeMeteorSprite.setPosition(-150, -350);
    playerSprite.setPosition(0, 230);
    enemySprite.setPosition(0, -300);

    enemyShotSprite.setPosition(enemySprite.getPosition().x + 20, enemySprite.getPosition().y);

    sf::CircleShape _playerHitCircle(25);
    sf::CircleShape _enemyHitCircle(25);
    sf::CircleShape _enemyShootCircle(10);
    sf::CircleShape _playerShootCircle(10);
    sf::CircleShape _smallMeteorCircle(10);
    sf::CircleShape _largeMeteorCircle(30);

    playerHitCircle = _playerHitCircle;
    enemyHitCircle = _enemyHitCircle;
    enemyShootCircle = _enemyShootCircle;
    playerShootCircle = _playerShootCircle;
    smallMeteorCircle = _smallMeteorCircle;
    largeMeteorCircle = _largeMeteorCircle;

    playerHitCircle.setFillColor(sf::Color(100, 250, 50));
    playerHitCircle.setPosition(playerSprite.getPosition().x, playerSprite.getPosition().y);

    enemyHitCircle.setFillColor(sf::Color(100, 250, 50));
    enemyHitCircle.setPosition(enemySprite.getPosition().x, enemySprite.getPosition().y);

    enemyShootCircle.setFillColor(sf::Color(100, 250, 50));
    enemyShootCircle.setPosition(enemyShotSprite.getPosition().x - 3, enemyShotSprite.getPosition().y);

    playerShootCircle.setFillColor(sf::Color(100, 250, 50));

    smallMeteorCircle.setFillColor(sf::Color(100, 250, 50));
    smallMeteorCircle.setPosition(smallMeteorSprite.getPosition().x, smallMeteorSprite.getPosition().y);

    largeMeteorCircle.setFillColor(sf::Color(100, 250, 50));
    largeMeteorCircle.setPosition(largeMeteorSprite.getPosition().x, largeMeteorSprite.getPosition().y);

    playerLaserHitsEnemy;
    enemyLaserHitsPlayer;
    playerColideWithEnemy;
    enemyColideWithPlayer;
    smallAstroidHitsPlayer;
    largeAstroidHitsPlayer;
    playerLaserHitsSmallMeteor;
    playerLaserHitsLargeMeteor;

    playerDead = false;
    enemyDead = false;

    ifGameOver = false;

    playerRespawnCounter = 90;
    enemyRespawnCounter = 90;

    gameOverCounter = 0;

    //This code, while it works, does not give the desired outcome. So has been left commented out.
    /*std::ifstream inputStream1("./Assets/PlayerData.json");
    std::string str1((std::istreambuf_iterator<char>(inputStream1)), std::istreambuf_iterator<char>());
    json::JSON document1 = json::JSON::Load(str1);

    std::ifstream inputStream2("./Assets/EnemyStartData.json");
    std::string str2((std::istreambuf_iterator<char>(inputStream2)), std::istreambuf_iterator<char>());
    json::JSON document2 = json::JSON::Load(str2);

    std::ifstream inputStream3("./Assets/WindowData.json");
    std::string str3((std::istreambuf_iterator<char>(inputStream3)), std::istreambuf_iterator<char>());
    json::JSON document3 = json::JSON::Load(str3);

    std::ifstream inputStream4("./Assets/SmallMeteorData.json");
    std::string str4((std::istreambuf_iterator<char>(inputStream4)), std::istreambuf_iterator<char>());
    json::JSON document4 = json::JSON::Load(str4);

    std::ifstream inputStream5("./Assets/LargeMeteorData.json");
    std::string str5((std::istreambuf_iterator<char>(inputStream5)), std::istreambuf_iterator<char>());
    json::JSON document5 = json::JSON::Load(str5);

    if (document3.hasKey("subObjectWindow"))
    {
        json::JSON subObject1 = document3["subObjectWindow"];
        if (subObject1.hasKey("windowXAxis"))
        {
            windowXAxis = subObject1["windowXAxis"].ToInt();
        }
        if (subObject1.hasKey("windowYAxis"))
        {
            windowYAxis = subObject1["windowYAxis"].ToInt();
        }
        if (subObject1.hasKey("ifGameOver"))
        {
            ifGameOver = subObject1["windowYAxis"].ToBool();
        }
        if (subObject1.hasKey("Font Characters Size"))
        {
            characterSize = subObject1["Font Characters Size"].ToInt();
        }
    }

    if (document1.hasKey("subObjectPlayer"))
    {
        json::JSON subObject2 = document1["subObjectPlayer"];
        if (subObject2.hasKey("livesNum"))
        {
            livesNum = subObject2["livesNum"].ToInt();

            livesString = "Lives Left: " + std::to_string(livesNum);

            livesText.setString(livesString);
        }
        if (subObject2.hasKey("scoreNum"))
        {
            scoreNum = subObject2["scoreNum"].ToInt();

            scoreString = "Score: " + std::to_string(scoreNum);

            scoreText.setString(scoreString);
        }
        if (subObject2.hasKey("PlayerPosX") && subObject2.hasKey("PlayerPosY"))
        {
            int x = subObject2["PlayerPosX"].ToInt();
            int y = subObject2["PlayerPosY"].ToInt();
            playerSprite.setPosition(x, y);
        }
        if (subObject2.hasKey("playerDead"))
        {
            playerDead = subObject2["playerDead"].ToBool();
        }
        if (subObject2.hasKey("playerRespawnCounter"))
        {
            playerRespawnCounter = subObject2["playerRespawnCounter"].ToInt();
        }
    }

    if (document2.hasKey("subObjectEnemy"))
    {
        json::JSON subObject3 = document2["subObjectEnemy"];
        if (subObject3.hasKey("EnemyPosX") && subObject3.hasKey("EnemyPosY"))
        {
            int x = subObject3["EnemyPosX"].ToInt();
            int y = subObject3["EnemyPosY"].ToInt();
            enemySprite.setPosition(x, y);
        }
        if (subObject3.hasKey("enemyDead"))
        {
            enemyDead = subObject3["enemyDead"].ToBool();
        }
        if (subObject3.hasKey("enemyRespawnCounter"))
        {
            enemyRespawnCounter = subObject3["enemyRespawnCounter"].ToInt();
        }
    }

    if (document4.hasKey("subObjectSmallMeteor"))
    {
        json::JSON subObject4 = document4["subObjectSmallMeteor"];
        if (subObject4.hasKey("smallMeteorPosX") && subObject4.hasKey("smallMeteorPosY"))
        {
            int x = subObject4["smallMeteorPosX"].ToInt();
            int y = subObject4["smallMeteorPosY"].ToInt();
            smallMeteorSprite.setPosition(x, y);
        }
    }

    if (document5.hasKey("subObjectLargeMeteor"))
    {
        json::JSON subObject5 = document5["subObjectLargeMeteor"];
        if (subObject5.hasKey("largeMeteorPosX") && subObject5.hasKey("largeMeteorPosY"))
        {
            int x = subObject5["largeMeteorPosX"].ToInt();
            int y = subObject5["largeMeteorPosY"].ToInt();
            largeMeteorSprite.setPosition(x, y);
        }
    }*/
}

//Run game runs all the games code on call, including the main game loop.
//The player may shoot meteors, avoid them, or be killed by them. While also 
//being able to kill the enemy ship, both by shootig it and ramming it
//with your own ship, but will destory you in the process. As well as the enemy
//ship being able to shoot its own lasers and kill the player. The enemy ship
//is not affected by meteors however. The player may exit the game at any time by
//pressing the esc key. As well as save the game with ~ and load it with L. The player
//moves with WASD and shoots with space. While the high score is collected at the end of a 
//finshed game, and presuved contuously. Only updating if the previous record was beaten. 
void Window::RunGame()
{
    while (window != nullptr)
    {
        while (window != nullptr && window->pollEvent(event))
        {
            switch (event.type)
            {
            case sf::Event::Closed:
                window->close();
                delete window;
                window = nullptr;
                break;

            case sf::Event::KeyPressed:
                if (event.key.code == sf::Keyboard::A && playerDead == false)
                {
                    playerSprite.move(-5, 0);
                    playerHitCircle.move(-5, 0);
                }
                else if (event.key.code == sf::Keyboard::D && playerDead == false)
                {
                    playerSprite.move(5, 0);
                    playerHitCircle.move(5, 0);
                }
                else if (event.key.code == sf::Keyboard::W && playerDead == false)
                {
                    playerSprite.move(0, -5);
                    playerHitCircle.move(0, -5);
                }
                else if (event.key.code == sf::Keyboard::S && playerDead == false)
                {
                    playerSprite.move(0, 5);
                    playerHitCircle.move(0, 5);
                }
                else if (event.key.code == sf::Keyboard::Space && playerDead == false)
                {
                    playerShotSprite.setPosition(playerSprite.getPosition().x + 21, playerSprite.getPosition().y + -10);
                    playerShootCircle.setPosition(playerShotSprite.getPosition().x - 8, playerShotSprite.getPosition().y - 3);
                    shoot = true;
                }
                else if (event.key.code == sf::Keyboard::Escape)
                {
                    window = nullptr;
                }
                else if (event.key.code == sf::Keyboard::L)
                {
                    std::ifstream inputStream("./Assets/SaveData.json");
                    std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
                    json::JSON document = json::JSON::Load(str);

                    if (document.hasKey("subObjectWindow"))
                    {
                        json::JSON subObject = document["subObjectWindow"];
                        if (subObject.hasKey("windowXAxis"))
                        {
                            windowXAxis = subObject["windowXAxis"].ToInt();
                        }
                        if (subObject.hasKey("windowYAxis"))
                        {
                            windowYAxis = subObject["windowYAxis"].ToInt();
                        }
                        if (subObject.hasKey("ifGameOver"))
                        {
                            ifGameOver = subObject["windowYAxis"].ToBool();
                        }
                        if (subObject.hasKey("Font Characters Size"))
                        {
                            characterSize = subObject["Font Characters Size"].ToInt();
                        }
                    }

                    if (document.hasKey("subObjectPlayer"))
                    {
                        json::JSON subObject = document["subObjectPlayer"];
                        if (subObject.hasKey("livesNum"))
                        {
                            livesNum = (subObject["livesNum"].ToInt());

                            livesString = ("Lives Left: " + std::to_string(livesNum));

                            livesText.setString(livesString);
                        }
                        if (subObject.hasKey("scoreNum"))
                        {
                            scoreNum = (subObject["scoreNum"].ToInt());

                            scoreString = ("Score: " + std::to_string(scoreNum));

                            scoreText.setString(scoreString);
                        }
                        if (subObject.hasKey("highscoreNum"))
                        {
                            highscoreNum = (subObject["highscoreNum"].ToInt());

                            highscoreString = ("High score: " + std::to_string(highscoreNum));

                            highScoreText.setString(highscoreString);
                        }
                        if (subObject.hasKey("PlayerPosX") && subObject.hasKey("PlayerPosY"))
                        {
                            int x = subObject["PlayerPosX"].ToInt();
                            int y = subObject["PlayerPosY"].ToInt();
                            playerSprite.setPosition(x, y);
                        }
                        if (subObject.hasKey("playerDead"))
                        {
                            playerDead = (subObject["playerDead"].ToBool());
                        }
                        if (subObject.hasKey("playerRespawnCounter"))
                        {
                            playerRespawnCounter = (subObject["playerRespawnCounter"].ToInt());
                        }
                    }

                    if (document.hasKey("subObjectEnemy"))
                    {
                        json::JSON subObject = document["subObjectEnemy"];
                        if (subObject.hasKey("EnemyPosX") && subObject.hasKey("EnemyPosY"))
                        {
                            int x = subObject["EnemyPosX"].ToInt();
                            int y = subObject["EnemyPosY"].ToInt();
                            enemySprite.setPosition(x, y);
                        }
                        if (subObject.hasKey("enemyDead"))
                        {
                            enemyDead = (subObject["enemyDead"].ToBool());
                        }
                        if (subObject.hasKey("enemyRespawnCounter"))
                        {
                            enemyRespawnCounter = (subObject["enemyRespawnCounter"].ToInt());
                        }
                    }

                    if (document.hasKey("subObjectSmallMeteor"))
                    {
                        json::JSON subObject = document["subObjectSmallMeteor"];
                        if (subObject.hasKey("smallMeteorPosX") && subObject.hasKey("smallMeteorPosY"))
                        {
                            int x = subObject["smallMeteorPosX"].ToInt();
                            int y = subObject["smallMeteorPosY"].ToInt();
                            smallMeteorSprite.setPosition(x, y);
                        }
                    }

                    if (document.hasKey("subObjectLargeMeteor"))
                    {
                        json::JSON subObject = document["subObjectLargeMeteor"];
                        if (subObject.hasKey("largeMeteorPosX") && subObject.hasKey("largeMeteorPosY"))
                        {
                            int x = subObject["largeMeteorPosX"].ToInt();
                            int y = subObject["largeMeteorPosY"].ToInt();
                            largeMeteorSprite.setPosition(x, y);
                        }
                    }
                }
                else if (event.key.code == sf::Keyboard::Tilde)
                {
                    json::JSON document;

                    json::JSON subObjectWindow = json::JSON::Object();
                    subObjectWindow["windowXAxis"] = windowXAxis;
                    subObjectWindow["windowYAxis"] = windowYAxis;
                    subObjectWindow["ifGameOver"] = ifGameOver;
                    subObjectWindow["Font Characters Size"] = characterSize;
                    document["subObjectWindow"] = subObjectWindow;

                    json::JSON subObjectPlayer = json::JSON::Object();
                    subObjectPlayer["livesNum"] = livesNum;
                    subObjectPlayer["scoreNum"] = scoreNum;
                    subObjectPlayer["highscoreNum"] = highscoreNum;
                    subObjectPlayer["PlayerPosX"] = playerSprite.getPosition().x;
                    subObjectPlayer["PlayerPosY"] = playerSprite.getPosition().y;
                    subObjectPlayer["playerDead"] = playerDead;
                    subObjectPlayer["playerRespawnCounter"] = playerRespawnCounter;
                    document["subObjectPlayer"] = subObjectPlayer;

                    json::JSON subObjectEnemy = json::JSON::Object();
                    subObjectEnemy["EnemyPosX"] = enemySprite.getPosition().x;
                    subObjectEnemy["EnemyPosY"] = enemySprite.getPosition().y;
                    subObjectEnemy["enemyDead"] = enemyDead;
                    subObjectEnemy["enemyRespawnCounter"] = enemyRespawnCounter;
                    document["subObjectEnemy"] = subObjectEnemy;

                    json::JSON subObjectSmallMeteor = json::JSON::Object();
                    subObjectSmallMeteor["smallMeteorPosX"] = smallMeteorSprite.getPosition().x;
                    subObjectSmallMeteor["smallMeteorPosY"] = smallMeteorSprite.getPosition().y;
                    document["subObjectSmallMeteor"] = subObjectSmallMeteor;

                    json::JSON subObjectLargeMeteor = json::JSON::Object();
                    subObjectLargeMeteor["largeMeteorPosX"] = largeMeteorSprite.getPosition().x;
                    subObjectLargeMeteor["largeMeteorPosY"] = largeMeteorSprite.getPosition().y;
                    document["subObjectLargeMeteor"] = subObjectLargeMeteor;

                    std::ofstream ostrm("./Assets/SaveData.json");
                    ostrm << document.dump() << std::endl;
                }
                break;

            default:
                break;
            }
        }

        if (enemyDead == false)
        {
            enemySprite.move(0, 1);
            enemyHitCircle.move(0, 1);
        }

        smallMeteorCircle.move(0, 3);
        smallMeteorCircle.rotate(0.5);
        smallMeteorSprite.move(0, 3);
        smallMeteorSprite.rotate(0.5);

        largeMeteorSprite.move(0, 3);
        largeMeteorSprite.rotate(0.5);
        largeMeteorCircle.move(0, 3);
        largeMeteorCircle.rotate(0.5);

        if (playerShotCounter <= 75 && shoot == true && playerDead == false)
        {
            playerShootCircle.move(0, -5);
            playerShotSprite.move(0, -5);
            playerShotCounter = playerShotCounter + 1;

        }
        else
        {
            shoot = false;
            playerShotCounter = 0;
            playerShootCircle.setPosition(playerShotSprite.getPosition().x, playerShotSprite.getPosition().y);
        }

        if (enemyShotCounter <= 75 && enemyDead == false)
        {
            enemyShotSprite.move(0, 5);
            enemyShootCircle.move(0, 5);
            enemyShotCounter = enemyShotCounter + 1;

        }
        else
        {
            enemyShotCounter = 0;
            enemyShotSprite.setPosition(enemySprite.getPosition().x + 20, enemySprite.getPosition().y);
            enemyShootCircle.setPosition(enemyShotSprite.getPosition().x - 3, enemyShotSprite.getPosition().y);
        }

        enemyLaserHitsPlayer = playerHitCircle.getGlobalBounds().intersects(enemyShootCircle.getGlobalBounds());

        if (enemyLaserHitsPlayer == true && playerDead == false)
        {
            playerSprite.setTexture(explostionTexture);
            playerSprite.setTexture(playerDeadTexture);
            livesNum = (livesNum - 1);
            livesString = ("Lives Left: " + std::to_string(livesNum));
            livesText.setString(livesString);
            playerDead = true;
        }

        playerColideWithEnemy = playerHitCircle.getGlobalBounds().intersects(enemyHitCircle.getGlobalBounds());
        enemyColideWithPlayer = enemyHitCircle.getGlobalBounds().intersects(playerHitCircle.getGlobalBounds());

        if (playerColideWithEnemy == true && enemyColideWithPlayer == true && playerDead == false && enemyDead == false)
        {
            playerSprite.setTexture(explostionTexture);
            playerSprite.setTexture(playerDeadTexture);
            livesNum = (livesNum - 1);
            livesString = ("Lives Left: " + std::to_string(livesNum));
            livesText.setString(livesString);
            playerDead = true;

            enemySprite.setTexture(explostionTexture);
            enemySprite.setTexture(playerDeadTexture);
            scoreNum = (scoreNum + 10);
            scoreString = ("Score: " + std::to_string(scoreNum));
            scoreText.setString(scoreString);
            enemyDead = true;
        }

        smallAstroidHitsPlayer = playerHitCircle.getGlobalBounds().intersects(smallMeteorCircle.getGlobalBounds());

        if (smallAstroidHitsPlayer == true && playerDead == false)
        {
            playerSprite.setTexture(explostionTexture);
            playerSprite.setTexture(playerDeadTexture);
            livesNum = (livesNum - 1);
            livesString = ("Lives Left: " + std::to_string(livesNum));
            livesText.setString(livesString);
            playerDead = true;

            smallMeteorSprite.setPosition(150, -500);
            smallMeteorCircle.setPosition(smallMeteorSprite.getPosition().x, smallMeteorSprite.getPosition().y);
        }

        largeAstroidHitsPlayer = playerHitCircle.getGlobalBounds().intersects(largeMeteorCircle.getGlobalBounds());

        if (largeAstroidHitsPlayer == true && playerDead == false)
        {
            playerSprite.setTexture(explostionTexture);
            playerSprite.setTexture(playerDeadTexture);
            livesNum = (livesNum - 1);
            livesString = ("Lives Left: " + std::to_string(livesNum));
            livesText.setString(livesString);
            playerDead = true;

            largeMeteorSprite.setPosition(-150, -550);
            largeMeteorCircle.setPosition(largeMeteorSprite.getPosition().x, largeMeteorSprite.getPosition().y);
        }


        playerLaserHitsEnemy = enemyHitCircle.getGlobalBounds().intersects(playerShootCircle.getGlobalBounds());

        if (playerLaserHitsEnemy == true && playerDead == false)
        {
            enemySprite.setTexture(explostionTexture);
            enemySprite.setTexture(playerDeadTexture);
            scoreNum = (scoreNum + 10);
            scoreString = ("Score: " + std::to_string(scoreNum));
            scoreText.setString(scoreString);
            enemyDead = true;
        }

        playerLaserHitsSmallMeteor = smallMeteorCircle.getGlobalBounds().intersects(playerShootCircle.getGlobalBounds());

        if (playerLaserHitsSmallMeteor == true)
        {
            smallMeteorSprite.setPosition(150, -500);
            smallMeteorCircle.setPosition(smallMeteorSprite.getPosition().x, smallMeteorSprite.getPosition().y);
            scoreNum = (scoreNum + 5);
            scoreString = ("Score: " + std::to_string(scoreNum));
            scoreText.setString(scoreString);
        }

        playerLaserHitsLargeMeteor = largeMeteorCircle.getGlobalBounds().intersects(playerShootCircle.getGlobalBounds());

        if (playerLaserHitsLargeMeteor == true)
        {
            largeMeteorSprite.setPosition(-150, -500);
            largeMeteorCircle.setPosition(largeMeteorSprite.getPosition().x, largeMeteorSprite.getPosition().y);
            scoreNum = (scoreNum + 5);
            scoreString = ("Score: " + std::to_string(scoreNum));
            scoreText.setString(scoreString);
        }

        if (playerRespawnCounter >= 0 && playerDead == true)
        {
            playerRespawnCounter = (playerRespawnCounter - 1);
            if (playerRespawnCounter == 0)
            {
                playerRespawnCounter = 90;
                playerDead = false;
                playerSprite.setTexture(playerTexture);
                playerSprite.setPosition(0, 230);
                playerHitCircle.setPosition(0, 230);
                playerLaserHitsEnemy = false;
            }
        }

        if (enemyRespawnCounter >= 0 && enemyDead == true)
        {
            enemyRespawnCounter = (enemyRespawnCounter - 1);
            if (enemyRespawnCounter == 0)
            {
                playerShootCircle.setPosition(playerShotSprite.getPosition().x - 8, playerShotSprite.getPosition().y - 3);
                enemyRespawnCounter = 90;
                enemyDead = false;
                enemySprite.setTexture(enemyTexture);
                enemySprite.setPosition(0, -300);
                enemyHitCircle.setPosition(0, -300);
                playerShotSprite.setPosition(playerSprite.getPosition().x + 21, playerSprite.getPosition().y + -10);
                playerShootCircle.setPosition(playerShotSprite.getPosition().x - 8, playerShotSprite.getPosition().y - 3);
                enemyLaserHitsPlayer = false;
            }
        }

        if (window != nullptr) {

            window->clear();

            // The commented code out below was used to show the ships, 
            //meteors, and lasers hit boxes. I have left this code here in case
            //you wanted to see them. 
            window->draw(backgroundSprite);

            //window->draw(playerHitCircle);

            //window->draw(enemyHitCircle);

            window->draw(playerSprite);

            //window->draw(smallMeteorCircle);

            //window->draw(largeMeteorCircle);

            if (playerShotCounter <= 75 && shoot == true)
            {
                window->draw(playerShotSprite);
                //window->draw(playerShootCircle);
            }

            if (enemyShotCounter <= 75)
            {
                window->draw(enemyShotSprite);
                //window->draw(enemyShootCircle);
            }

            window->draw(smallMeteorSprite);
            window->draw(largeMeteorSprite);

            window->draw(enemySprite);

            window->draw(livesText);
            window->draw(scoreText);
            window->draw(highScoreText);

            if (livesNum <= 0)
            {
                if (scoreNum > highscoreNum)
                {
                    json::JSON document;

                    json::JSON subObjectPlayer = json::JSON::Object();
                    highscoreNum = (scoreNum);
                    subObjectPlayer["highscoreNum"] = highscoreNum;
                    document["subObjectPlayer"] = subObjectPlayer;

                    std::ofstream ostrm("./Assets/SaveData.json");
                    ostrm << document.dump() << std::endl;
                }

                window->draw(gameOverText);
                ifGameOver = true;
            }

            if (livesNum <= 0 && ifGameOver == true)
            {
                gameOverCounter = gameOverCounter + 1;
                if (gameOverCounter == 75)
                {
                    break;
                }

            }

            window->display();


        }
    }
}