#include <iostream>
#include <SFML/System.hpp>
#include<SFML/Graphics.hpp>
#include <string> 
#include <iostream>
#include <fstream>

/// <summary>
/// This is the header file for all meteor related data
/// </summary>
class Meteor
{
public:
	Meteor();
	~Meteor();
	void SetSmallMeteorTexture(sf::Texture texture);
	void SetLargeMeteorTexture(sf::Texture texture);
	sf::Texture GetSmallMeteorTexture() { return smallMeteorTexture; };
	sf::Texture GetLargeMeteorTexture() { return smallMeteorTexture; };

private:
	sf::Texture smallMeteorTexture;
	sf::Texture largeMeteorTexture;
};

