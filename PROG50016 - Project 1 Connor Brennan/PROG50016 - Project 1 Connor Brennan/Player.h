#include <string> 

/// <summary>
/// This is the header file for all player related data
/// </summary>
class Player
{
public:
	Player();
	~Player();
	std::string GetLivesString() { return livesString; };
	std::string GetScoreString() { return scoreString; };
	std::string GetHighscoreString() { return highscoreString; };
	int GetLivesNum() { return livesNum = 3; };
	int GetScoreNum() { return scoreNum = 0; };
	int GetHighscoreNum() { return highscoreNum = 0; };
	void SetLivesNum(int _livesNum);
	void SetScoreNum(int _scoreNum);
	void SetHighscoreNum(int _HscoreNum);
	void SetLivesString(std::string _Lstring);
	void SetScoreString(std::string _Sstring);
	void SetHighscoreString(std::string _Hstring);
	bool GetPlayerDead() { return playerDead; };
	void SetPlayerDead(bool _pDead);
	int GetRespawnCounter() { return playerRespawnCounter; };
	void SetRespawnCounter(int _counter);

private:
	int livesNum = 3;
	std::string livesString;
	int scoreNum = 0;
	std::string scoreString;
	int highscoreNum = 0;
	std::string highscoreString;
	bool playerDead;
	int playerRespawnCounter;

};

