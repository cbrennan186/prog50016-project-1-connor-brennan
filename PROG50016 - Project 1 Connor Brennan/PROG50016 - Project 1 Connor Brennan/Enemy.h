#pragma once

/// <summary>
/// This class is the header file for all enemy related data
/// </summary>
class Enemy
{
public:
	Enemy();
	~Enemy();
	void SetEnemyDead(bool _dead);
	void SetEnemyRespawnCounter(int _counter);
	bool GetEnemyDead() { return enemyDead; };
	bool GetEnemyRespawnCounter() { return enemyRespawnCounter; };

private:
	bool enemyDead;
	int enemyRespawnCounter;
};

